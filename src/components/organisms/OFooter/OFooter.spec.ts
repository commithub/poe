import { newSpecPage } from "@stencil/core/testing";
import { OFooter } from "./OFooter";

describe("OFooter", () => {
  it("renders with the correct props", async () => {
    const { root } = await newSpecPage({
      components: [OFooter],
      html: `<Footer country="Detroit, US" name="Ori Morningstar" url="https://orimorningstar.com"></Footer>`,
    });
    expect(root)
      .toEqualHtml(`<footer country="Detroit, US" name="Ori Morningstar" url="https://orimorningstar.com"></footer>
    `);
  });
});
