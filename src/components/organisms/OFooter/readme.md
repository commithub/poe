# m-footer

<!-- Auto Generated Below -->

## Properties

| Property                 | Attribute           | Description                                              | Type     | Default     |
| ------------------------ | ------------------- | -------------------------------------------------------- | -------- | ----------- |
| `brandDescription`       | `brand-description` | Description of the brand                                 | `string` | `undefined` |
| `brandLogo` _(required)_ | `brand-logo`        | URL of the logo for the brand                            | `string` | `undefined` |
| `country` _(required)_   | `country`           | Country to be added in copyright notice                  | `string` | `undefined` |
| `directory`              | `directory`         | JSON string of urls for the main directory of the footer | `string` | `undefined` |
| `name` _(required)_      | `name`              | Name of the website                                      | `string` | `undefined` |
| `socialAccounts`         | `social-accounts`   | JSON string of urls for the social accounts.             | `string` | `undefined` |
| `testid` _(required)_    | `testid`            | Automated test id                                        | `string` | `undefined` |

## Dependencies

### Depends on

- [poe-m-link-group](../../molecules/MLinkGroup)

### Graph

```mermaid
graph TD;
  poe-o-footer --> poe-m-link-group
  poe-m-link-group --> poe-a-link
  style poe-o-footer fill:#f9f,stroke:#333,stroke-width:4px
```

---

_Built with [StencilJS](https://stenciljs.com/)_
