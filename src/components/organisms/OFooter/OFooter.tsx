import { Component, Prop, h } from "@stencil/core";
import getCurrentYear from "../../../modules/get-current-year";

@Component({
  tag: "poe-o-footer",
  styleUrl: "OFooter.scss",
  shadow: false,
})

/**
 * HTML element represents a footer for its nearest ancestor sectioning content or sectioning
 * root element. A <footer> typically contains information about the author of the section,
 * copyright data or links to related documents.
 * @see [MDN: Footer Element](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/footer)
 */
export class OFooter {
  /**
   * Automated test id
   */
  @Prop() testid!: string;
  /**
   * Country to be added in copyright notice
   */
  @Prop() country!: string;
  /**
   * Name of the website
   */
  @Prop() name!: string;
  /**
   *  URL of the logo for the brand
   */
  @Prop() brandLogo!: string;
  /**
   *  Description of the brand
   */
  @Prop() brandDescription: string;
  /**
   * JSON string of urls for the main directory of the footer
   */
  @Prop() directory: string;
  /**
   * JSON string of urls for the social accounts.
   */
  @Prop() socialAccounts: string;
  /**
   * The current year for the copyright notice
   * @private
   */
  private currentYear: number = getCurrentYear();

  render() {
    return (
      <footer data-testid={this.testid} class="poe-o-footer">
        <div class="poe-o-footer-main-content poe-o-footer-container-spacing">
          <div class="poe-o-footer-main-content-brand poe-o-footer-container-spacing">
            <img
              class="poe-o-footer-main-content-brand-image"
              src={this.brandLogo}
              alt={`${this.name} brand logo`}
            />
            {this.brandDescription ? <p>{this.brandDescription}</p> : null}
            <poe-m-link-group links={this.socialAccounts}></poe-m-link-group>
          </div>
          <div class="poe-o-footer-main-content-directory">
            <p class="paragraph-bold">Directory</p>
            <poe-m-link-group links={this.directory}></poe-m-link-group>
          </div>
        </div>
        <div class="poe-o-footer-copyright poe-o-footer-container-spacing">
          <p>
            Made with 💖 in {this.country} &copy; {this.currentYear}
          </p>
        </div>
      </footer>
    );
  }
}
