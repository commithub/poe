import { Component, Event, EventEmitter, Prop, State, h } from "@stencil/core";

@Component({
  tag: "poe-a-toggle",
  styleUrl: "a-toggle.scss",
  shadow: false,
})
/**
 * HTML checkbox input type element that corresponds to a truthy or falsy value
 * @see [MDN: Checkbox](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Input/checkbox)
 */
export class AToggle {
  /**
   * Automated testid
   */
  @Prop() testid!: string;
  /**
   * Identifies the input
   */
  @Prop() label!: string;
  /**
   * Identify the two states of the toggle element
   */
  @Prop() titles!: string;
  /**
   * Adds disabled property
   * @see [MDN: HTML attribute: disabled](https://developer.mozilla.org/en-US/docs/Web/HTML/Attributes/disabled)
   */
  @Prop() isDisabled: boolean = false;
  /**
   * Registers event for when there is a click action
   * @see [Stenciljs Event](https://stenciljs.com/docs/events#events)
   */
  /**
   * Allows the user to determine if the checkbox initially is checked
   * @see [MDN: Checkbox](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Input/checkbox)
   */
  @Prop() isChecked: boolean = false;
  /**
   * It determines if the checkbox is checked
   * @see [MDN: Checkbox](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/Input/checkbox)
   */
  @State() checked: boolean = this.isChecked;

  @Event({
    eventName: "poeAToggleClicked",
    composed: true,
    cancelable: true,
    bubbles: true,
  })
  poeAToggleClicked: EventEmitter<object>;

  /**
   * Prevents default behavior for form submissions such as a page refresh and emits an event
   * @private
   * @function onClickHandler
   */
  private onClickHandler(event: Event): void {
    event.preventDefault();
    this.poeAToggleClicked.emit(event);
    this.checked = !this.checked;
  }

  /**
   * Parsed array of the titles
   */
  private parsedTitles: string[] = [];

  /**
   * Called once just after the component is first connected to the DOM.
   * @see [StencilJS componentWillLoad](https://stenciljs.com/docs/component-lifecycle#componentwillload)
   */
  componentWillLoad(): void {
    this.parsedTitles = JSON.parse(this.titles);
    return;
  }

  /**
   * Renders the UI with the appropriate state
   * @public
   * @function render
   * @see [StencilJS rendering state](https://stenciljs.com/docs/component-lifecycle#rendering-state)
   */
  render() {
    return (
      <div class="poe-a-toggle">
        <p>{this.parsedTitles[0]}</p>
        <label class="poe-a-toggle-switch">
          <p class="poe-a-toggle-label">{this.label}</p>
          <input
            type="checkbox"
            data-testid={this.testid}
            checked={this.checked}
            onClick={(event) => this.onClickHandler(event)}
          />
          <span
            class={`poe-a-toggle-slider poe-a-toggle-slider-condition-${this.checked}`}
          ></span>
        </label>
        <p>{this.parsedTitles[1]}</p>
      </div>
    );
  }
}
