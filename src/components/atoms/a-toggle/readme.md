# poe-a-toggle

<!-- Auto Generated Below -->

## Properties

| Property              | Attribute     | Description                                                       | Type      | Default     |
| --------------------- | ------------- | ----------------------------------------------------------------- | --------- | ----------- |
| `isChecked`           | `is-checked`  | Allows the user to determine if the checkbox initially is checked | `boolean` | `false`     |
| `isDisabled`          | `is-disabled` | Adds disabled property                                            | `boolean` | `false`     |
| `label` _(required)_  | `label`       | Identifies the input                                              | `string`  | `undefined` |
| `testid` _(required)_ | `testid`      | Automated testid                                                  | `string`  | `undefined` |
| `titles` _(required)_ | `titles`      | Identify the two states of the toggle element                     | `string`  | `undefined` |

## Events

| Event               | Description | Type                  |
| ------------------- | ----------- | --------------------- |
| `poeAToggleClicked` |             | `CustomEvent<object>` |

---

_Built with [StencilJS](https://stenciljs.com/)_
