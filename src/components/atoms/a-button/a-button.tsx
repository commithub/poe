import { Component, Event, EventEmitter, Prop, h } from "@stencil/core";
import { oneOfMany } from "../../../modules/validators";

@Component({
  tag: "poe-a-button",
  styleUrl: "a-button.scss",
  shadow: false,
})
/**
 * HTML element is an interactive element activated by a user with a mouse,
 * keyboard, finger, voice command, or other assistive technology.
 * Once activated, it then performs a programmable action,
 * such as submitting a form or opening a dialog.
 * @see [MDN: Button Element](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/button)
 */
export class AButton {
  /**
   * Automated testid
   */
  @Prop() testid!: string;
  /**
   * Default behavior
   * @see [MDN: Button Element Attribute Type](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/button#attr-type)
   */
  @Prop() type: string = "button";
  /**
   * Expands the width to 100%
   */
  @Prop() isFullWidth: boolean = false;
  /**
   * Adds disabled property
   * @see [MDN: HTML attribute: disabled](https://developer.mozilla.org/en-US/docs/Web/HTML/Attributes/disabled)
   */
  @Prop() isDisabled: boolean = false;
  /**
   * Changes the background color
   */
  @Prop() color: string = "primary";
  /**
   * Changes styles like border
   */
  @Prop() isOutlined: boolean = false;

  /**
   * Registers event for when there is a click action
   * @see [Stenciljs Event](https://stenciljs.com/docs/events#events)
   */
  @Event({
    eventName: "poeAButtonClicked",
    composed: true,
    cancelable: true,
    bubbles: true,
  })
  poeAButtonClicked: EventEmitter<object>;

  /**
   * Retrieves the color className
   * @public
   */
  get colorClassName(): string {
    return `-color-${this.color}`;
  }

  /**
   * Retrieves the isFullWidth className
   * @public
   */
  get isFullWidthClassName(): string {
    if (this.isFullWidth) {
      return `poe-a-button-fullwidth`;
    } else {
      return "";
    }
  }

  /**
   * Retrieves the isOutlined className
   * @public
   */
  get isOutlinedClassName(): string {
    if (this.isOutlined) {
      return `-outlined`;
    } else {
      return "";
    }
  }

  /**
   * Prevents default behavior for form submissions such as a page refresh and emits an event
   * @private
   * @function onClickHandler
   */
  private onClickHandler(event: Event): void {
    event.preventDefault();
    this.poeAButtonClicked.emit(event);
  }

  /**
   * Validates the different props
   * @private
   * @function validateProps
   * @throws displays an error on the console on which prop is invalid
   */
  private validateProps(): void {
    try {
      oneOfMany(this.type, ["button", "submit", "reset"]);
      oneOfMany(this.color, [
        "primary",
        "warning",
        "danger",
        "info",
        "success",
        "neutral",
      ]);
    } catch (err) {
      console.error(err);
    }
  }

  /**
   * Called once just after the component is first connected to the DOM. Since this method is only
   * called once, it's a good place to load data asynchronously and to setup the state without triggering extra re-renders.
   * A promise can be returned, that can be used to wait for the first render().
   * @public
   * @function componentWillLoad
   * @see [StencilJS componentWillLoad Lifecycle Method](https://stenciljs.com/docs/component-lifecycle#componentwillload)
   */
  componentWillLoad(): void {
    this.validateProps();
  }

  /**
   * Renders the UI with the appropriate state
   * @public
   * @function render
   * @see [StencilJS rendering state](https://stenciljs.com/docs/component-lifecycle#rendering-state)
   */
  render() {
    return (
      <button
        type={this.type}
        data-testid={this.testid}
        disabled={this.isDisabled}
        class={`poe-a-button${this.colorClassName}${this.isOutlinedClassName} ${this.isFullWidthClassName}`}
        onClick={(event) => this.onClickHandler(event)}
      >
        <slot />
      </button>
    );
  }
}
