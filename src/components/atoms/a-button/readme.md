# poe-a-button

<!-- Auto Generated Below -->

## Properties

| Property              | Attribute       | Description                  | Type      | Default     |
| --------------------- | --------------- | ---------------------------- | --------- | ----------- |
| `color`               | `color`         | Changes the background color | `string`  | `"primary"` |
| `isDisabled`          | `is-disabled`   | Adds disabled property       | `boolean` | `false`     |
| `isFullWidth`         | `is-full-width` | Expands the width to 100%    | `boolean` | `false`     |
| `isOutlined`          | `is-outlined`   | Changes styles like border   | `boolean` | `false`     |
| `testid` _(required)_ | `testid`        | Automated testid             | `string`  | `undefined` |
| `type`                | `type`          | Default behavior             | `string`  | `"button"`  |

## Events

| Event               | Description                                      | Type                  |
| ------------------- | ------------------------------------------------ | --------------------- |
| `poeAButtonClicked` | Registers event for when there is a click action | `CustomEvent<object>` |

---

_Built with [StencilJS](https://stenciljs.com/)_
