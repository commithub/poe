import { Component, Prop, h } from "@stencil/core";
import { oneOfMany } from "../../../modules/validators";

@Component({
  tag: "poe-a-spinner",
  styleUrl: "a-spinner.scss",
  shadow: false,
})

/**
 * Spinner component to indicate a loading procedure
 * @class ASpinner
 * @see [Stenciljs: components](https://stenciljs.com/docs/component)
 */
export class ASpinner {
  /**
   * Changes the background color of the button
   */
  @Prop() styleType: string = "primary";
  /** Modifies the spinner size
   */
  @Prop() size: string = "medium";
  /**
   * id for automation tests
   */
  @Prop() testid!: string;

  /**
   * Retrieves the styleType className
   * @public
   */
  get styleTypeClassName(): string {
    return `poe-a-spinner-style-type-${this.styleType}`;
  }

  /**
   * Retrieves the size className
   * @public
   */
  get sizeClassName(): string {
    return `poe-a-spinner-size-${this.size}`;
  }

  /**
   * Validates the different props
   * @private
   * @function validateProps
   * @throws displays an error on the console on which prop is invalid
   */
  private validateProps(): void {
    try {
      oneOfMany(this.styleType, [
        "primary",
        "warning",
        "danger",
        "info",
        "success",
        "neutral-10",
      ]);
      oneOfMany(this.size, ["small", "medium", "large"]);
    } catch (err) {
      return console.error(err);
    }
  }

  /**
   * Called once just after the component is first connected to the DOM. Since this method is only
   * called once, it's a good place to load data asynchronously and to setup the state without triggering extra re-renders.
   * A promise can be returned, that can be used to wait for the first render().
   * @public
   * @function componentWillLoad
   * @see [StencilJS componentWillLoad Lifecycle Method](https://stenciljs.com/docs/component-lifecycle#componentwillload)
   */
  componentWillLoad(): void {
    return this.validateProps();
  }

  /**
   * Renders the UI with the appropriate state
   * @public
   * @function render
   * @see [StencilJS rendering state](https://stenciljs.com/docs/component-lifecycle#rendering-state)
   */
  render() {
    return (
      <div
        data-testid={this.testid}
        class={`poe-a-spinner ${this.sizeClassName}`}
      >
        <div class={this.styleTypeClassName}></div>
      </div>
    );
  }
}
