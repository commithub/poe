import { Component, Prop, h } from "@stencil/core";
import { brands, solid } from "../../../modules/font-awesome";
import { IIconLibrary, IIconEntry } from "./interfaces";

@Component({
  tag: "poe-a-link",
  styleUrl: "ALink.scss",
  shadow: false,
})
export class ALink {
  @Prop() icon: string;
  @Prop() label: string;
  @Prop() url: string;

  private iconLibrary: IIconLibrary = {
    gitlab: {
      type: "brand",
      icon: brands.gitlab,
      alt: "GitLab icon",
    },
    externalLink: {
      type: "solid",
      icon: solid.externalLink,
      alt: "External link",
    },
  };

  get iconEntry(): IIconEntry | Record<string, never> {
    return this.icon ? this.iconLibrary[this.icon] : {};
  }

  render() {
    return (
      <a class="a-link" href={this.url}>
        <span class={`a-link-${this.iconEntry.type}-label`}>{this.label} </span>
        {this.iconEntry.icon ? (
          <img src={this.iconEntry.icon} alt={this.iconEntry.alt} />
        ) : null}
      </a>
    );
  }
}
