interface IIconEntry {
  type: string;
  icon: string;
  alt: string;
}

export default IIconEntry;
