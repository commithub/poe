interface ILinkGroup {
  label: string;
  url: string;
  icon?: string;
}

export default ILinkGroup;
