# poe-m-link-group

<!-- Auto Generated Below -->

## Properties

| Property | Attribute | Description | Type     | Default     |
| -------- | --------- | ----------- | -------- | ----------- |
| `links`  | `links`   |             | `string` | `undefined` |

## Dependencies

### Used by

- [poe-o-footer](../../organisms/OFooter)

### Depends on

- [poe-a-link](../../atoms/ALink)

### Graph

```mermaid
graph TD;
  poe-m-link-group --> poe-a-link
  poe-o-footer --> poe-m-link-group
  style poe-m-link-group fill:#f9f,stroke:#333,stroke-width:4px
```

---

_Built with [StencilJS](https://stenciljs.com/)_
