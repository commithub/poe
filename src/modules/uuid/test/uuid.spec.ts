import { generateUUID, validateUUID } from "../uuid";

describe("uuid", () => {
  describe("generateUUID", () => {
    it("generates a UUID", () => {
      const id = generateUUID();

      expect(typeof id).toEqual("string");
      expect(validateUUID(id)).toBeTruthy;
    });
  });

  describe("validateUUID", () => {
    it("returns true if UUID is valid", () => {
      expect(validateUUID("3e8cd438-ba32-45ab-bda3-5c7461de29a2")).toBeTruthy;
      expect(validateUUID(generateUUID())).toBeTruthy;
    });

    it("returns false if UUID is invalid", () => {
      expect(validateUUID("3ed438-ba32-45ab-bda3-5c7461de29a2")).toBeFalsy;
      expect(validateUUID("foo")).toBeFalsy;
    });
  });
});
