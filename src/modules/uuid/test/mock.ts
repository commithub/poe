export default function () {
  return jest.mock("../uuid", () => ({
    generateUUID: () => "adf84ab6-90fd-44f6-955d-23fbf6ec2855",
  }));
}
