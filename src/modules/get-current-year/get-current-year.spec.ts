import getCurrentYear from "./index";

describe("getCurrentYear", () => {
  it("gets the current year", () => {
    expect(getCurrentYear()).toEqual(new Date().getFullYear());
  });
});
