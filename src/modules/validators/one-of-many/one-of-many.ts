/**
 * Checks if the valueToValidate has a match with the validators
 */
export function oneOfMany(
  valueToValidate: string | number | boolean,
  validators: Array<string | number | boolean>
): boolean {
  const isValid = validators.find((validator) => valueToValidate === validator);
  if (isValid) return true;
  throw new Error(generateErrorMessage(valueToValidate, validators));
}

export function generateErrorMessage(
  valueToValidate: string | number | boolean,
  validators: Array<string | number | boolean>
): string {
  return `[oneOfMany]: ${valueToValidate} is not one of ${validators}`;
}
